#!/bin/bash

PROJECT="Seescan CCU"
RFSFILE="../../uploads/seescan-image-ccu-dev-csplus_cm.tar.bz2"
VERSION="develop"
KNLFILE="../../uploads/uImage_imx_v7_defconfig"
FILE=${RFSFILE}

setup_board() {
    echo "<p> Setting up board...<br/><p/>"
    sudo rm -r ${BOARD_WORKSPACE}/nfs/* &> /dev/null
    mkdir ${BOARD_WORKSPACE}/nfs &> /dev/null
    sudo tar -xf ${RFSFILE} -C ${BOARD_WORKSPACE}/nfs
    sudo sh -c 'echo "TimeoutSec=15" >> ../nfs/lib/systemd/system/systemd-modules-load.service'

    rm ../tftp/uImage &> /dev/null
    cp ${KNLFILE} ../tftp/uImage
}

inter_test_cleanup() {
    cd output/

    if [ "${TEST_NAME:-0}" == 0 ]; then
        OUTOUT_DIR=$(basename $script)
    else
        OUTPUT_DIR=${TEST_NAME// /_}
    fi

    mkdir ${OUTPUT_DIR}
    mv $(ls -A | grep -v ${OUTPUT_DIR}) ${OUTPUT_DIR}
    cd ../
}

board_cleanup() {
    echo "<p>Cleaning up the board area, nfs/tftp etc <br/>"
    sudo rm -r ${BOARD_WORKSPACE}/nfs/*
    rm ../tftp/uImage

    echo "Package up test logs and output <br/><p/>"
    TEST_RESULTS=../../test_results/$(date -u -Iminutes)
    mkdir -p ${TEST_RESULTS}
    tar -czf ${TEST_RESULTS}/results.tar.gz output/* logs/* &> /dev/null
    rm -r output/* &> /dev/null
    rm -r logs/* &> /dev/null
}
