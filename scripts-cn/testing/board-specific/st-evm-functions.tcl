proc board_sleep {count} {
    send -s "sleep $count && echo ebfarm_sleep_off\r"
    expect {
        timeout {
            puts " -- Board sleep failed?"
            return 1
        }
        "*#*"
    }
}

proc st_screen_grab {} {
    # clear prompt
    send -s "\r\r\r"

    # Send gst pipeline...
    send -s "gst-launch-1.0 multifilesrc num-buffers=1 location=/dev/fb0 ! videoparse format=5 width=640 height=1440 interlaced=true ! deinterlace ! videocrop bottom=960 ! videoconvert ! \"video/x-raw,format=ARGB\" ! queue ! videomixer name=mix ! videoconvert ! jpegenc quality=100 ! image/jpeg ! filesink location=test.jpg multifilesrc num-buffers=1 location=/dev/fb1 ! videoparse format=8 width=640 height=960 interlaced=true ! deinterlace ! videocrop bottom=480 ! videoconvert ! \"video/x-raw,format=RGBx\" ! alpha method=custom target_r=0 target_g=0 target_b=8 angle=10 black-sensitivity=128 ! \"video/x-raw,format=ARGB\" ! mix. \r"

    expect {
        timeout {
            puts " -- Failed to capture screengrab"
            return 1
        }
        "Freeing pipeline ..."
    }
    return 0    
}

proc st_keypress {key} {
    # clear prompt
    send -s "\r\r\r"
    send -s "echo $key | keypress \r"
    expect {
        timeout {
            puts "-- Keypress failed"
            return 1
        }
        "Complete"
    }
    return 0
}

proc st_enter_vidrec_screen {{first_boot 0} {debug 0}} {
    # Escape without entering serial number
    if { $debug == 1 } {
        st_screen_grab
        get_file_ssh "root" "10.150.7.144" "/home/root/test.jpg" "output/test-enter-vidrec-1.jpg"
    }
    st_keypress "esc"
    board_sleep 5

    # Enter to say no serial number is fine
    if { $debug == 1 } {
        st_screen_grab
        get_file_ssh "root" "10.150.7.144" "/home/root/test.jpg" "output/test-enter-vidrec-2.jpg"
    }    
    st_keypress "enter"
    board_sleep 15

    if { $first_boot == 1 }  {
        # Bypass first-boot setup screens
        if { $debug == 1 } {
            st_screen_grab
            get_file_ssh "root" "10.150.7.144" "/home/root/test.jpg" "output/test-enter-vidrec-firstboot-3.jpg"
        }    
        st_keypress "ar"
        board_sleep 5

        if { $debug == 1 } {
            st_screen_grab
            get_file_ssh "root" "10.150.7.144" "/home/root/test.jpg" "output/test-enter-vidrec-firstboot-4.jpg"
        }    
        st_keypress "ar"
        board_sleep 5

        if { $debug == 1 } {
            st_screen_grab
            get_file_ssh "root" "10.150.7.144" "/home/root/test.jpg" "output/test-enter-vidrec-firstboot-5.jpg"
        }    
        st_keypress "ar"
        board_sleep 5

        if { $debug == 1 } {
            st_screen_grab
            get_file_ssh "root" "10.150.7.144" "/home/root/test.jpg" "output/test-enter-vidrec-firstboot-6.jpg"
        }    
        st_keypress "enter"
        board_sleep 5

        if { $debug == 1 } {
            st_screen_grab
            get_file_ssh "root" "10.150.7.144" "/home/root/test.jpg" "output/test-enter-vidrec-firstboot-7.jpg"
        }    
        st_keypress "enter"
        board_sleep 5

        if { $debug == 1 } {
            st_screen_grab
            get_file_ssh "root" "10.150.7.144" "/home/root/test.jpg" "output/test-enter-vidrec-firstboot-8.jpg"
        }    
        st_keypress "enter"
        board_sleep 5
    }

    # Bypass unsafe shutdown screen with enter
    if { $debug == 1 } {
        st_screen_grab
        get_file_ssh "root" "10.150.7.144" "/home/root/test.jpg" "output/test-enter-vidrec-9.jpg"
    }
    st_keypress "enter"
    board_sleep 5

    # Escape the menu/settings screen
    if { $debug == 1 } {
        st_screen_grab
        get_file_ssh "root" "10.150.7.144" "/home/root/test.jpg" "output/test-enter-vidrec-10.jpg"
    }
    st_keypress "esc"
    board_sleep 5

    if { $first_boot == 1} {
        # Need to escape twice on first boot?
        if { $debug == 1 } {
            st_screen_grab
            get_file_ssh "root" "10.150.7.144" "/home/root/test.jpg" "output/test-enter-vidrec-firstboot-11.jpg"
        }
        st_keypress "esc"
        board_sleep 5
    }

    # Get screenshot to confirm we are on the video recording screen
    if { $debug == 1 } {
        st_screen_grab
        get_file_ssh "root" "10.150.7.144" "/home/root/test.jpg" "output/test-enter-vidrec-12.jpg"
    }
}

proc st_start_recording {{debug 0}} {
    # Press F2 to start
    st_keypress "f2"
    if { $debug == 1 } {
        st_screen_grab
        get_file_ssh "root" "10.150.7.144" "/home/root/test.jpg" "output/test-start-rec-1.jpg"
    }
    board_sleep 5

    # Enter to create new job
    st_keypress "enter"
    if { $debug == 1 } {
        st_screen_grab
        get_file_ssh "root" "10.150.7.144" "/home/root/test.jpg" "output/test-start-rec-2.jpg"
    }
    board_sleep 5

    # Enter name screen, 'skip' by enter with no name
    st_keypress "enter"
    st_keypress "enter"
    if { $debug == 1 } {
        st_screen_grab
        get_file_ssh "root" "10.150.7.144" "/home/root/test.jpg" "output/test-start-rec-3.jpg"
    }
}

proc st_stop_recording {{debug 0}} {
    # Press F2 to start
    st_keypress "f2"
    if { $debug == 1 } {
        board_sleep 2
        st_screen_grab
        get_file_ssh "root" "10.150.7.144" "/home/root/test.jpg" "output/test-stop-rec.jpg"
    }
    board_sleep 5
}
