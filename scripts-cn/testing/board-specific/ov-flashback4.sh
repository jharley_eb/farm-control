#!/bin/bash

# Ensure we have been given only one commission pack
FB4_COMMISSION_PACK_PREFIX="fb4_commission_pack_v"
NUM_FILES=`ls dropbox/$FB4_COMMISSION_PACK_PREFIX*.zip | wc -l`
if [ "$NUM_FILES" != "1" ] ; then
	echo "Multiple commissioning card images provided, aborting"
	exit 1
fi

# Parse the file name to determine the software version
FILE=`ls dropbox/$FB4_COMMISSION_PACK_PREFIX*.zip`
VERSION=`A=$FILE && B=\${A%.*} && C=\${B##*$FB4_COMMISSION_PACK_PREFIX} && echo \$C`

# Test functions
setup_board() {
    echo "No board setup required for ov-flashback4"
}

inter_test_cleanup() {
    echo "No inter-test cleanup required for ov-flashback4"
}

board_cleanup() {
    # Tar up the logs
    tar -czf `ls dropbox/fb4_commission_pack_*.zip`.boot_tests_logs.tar.gz logs/
}
