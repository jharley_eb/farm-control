proc commission_fb4 {image_file} {
	puts "--Powering down board to start commissioning process"
    ebfarm_cmd pwr off
	exec sh -c "sleep 5"

    setup_sd "mbr-recovery"
    mkfs_sd "vfat"
    setup_sd $image_file

	puts "--Powering board to start commissioning"
    ebfarm_boot_board 120
	
	set timeout 300
	expect {
		timeout {
			exit 1
		}
		"*==== Will reboot when sdcard removed ===="
	}
	puts "--Unit commissioned"

	# We can't just remove the card as that isn't detected by the FB4
	puts "--Turning board off"
	ebfarm_cmd pwr off

    puts "--Cleaning and reverting SD card setup"
    setup_sd "mbr-normal"
    mkfs_sd "vfat"

	puts "--Giving SD card back to board"
	ebfarm_cmd sd board
}

# This proc copys an update image to an SD card, inserts it into a FB4,
# turns on the board and verifes the unit is updated and gets to a prompt.
# There is no version checking here. 
proc update_fb4 {image_comm} {
	puts "--Turning board off, giving SD card to farm"
	ebfarm_cmd pwr off

	puts "--Figure out where the SD card is mounted"
	set output [ebfarm_cmd sd host]
	set mnt [exec sh -c "echo '$output' | grep ' at ' | rev | cut -d ' ' -f 1 | rev"]

    puts "--Removing any existing updates"
	exec bash -c "\[ -z '$mnt' \] || rm -rf $mnt/*.tgz*"

    puts "--Copy update tgz ($image_comm) to SD Card"
    copy_to_sd $image_comm

	puts "--Giving the card back to the board"
	ebfarm_cmd sd board
	exec sh -c "sleep 5"

	puts "--Powering board to start update"
	ebfarm_cmd pwr reboot

    ebfarm_boot_board 120
	puts "--Found kernel prompt (pre-update)"

	# Allow time for update
    ebfarm_boot_board 300
	puts "--Found kernel prompt (post-update)"

	# We may have got those kernel prompts too soon if the hardware kept
	# rebooting due to hardware issues, thus to be on the safe side wait
	# a while so that we dont reboot during an AVR upgrade
	exec sh -c "sleep 60"

	puts "--Unit updated"
}


proc commission_and_update { comm_file } {
	# Commission to old version

	set path [exec sh -c "ls releases/$comm_file"]
	puts "--Commission with earlier version of software ($path)"
	commission_fb4 $path
	
	# Turning board on for 30 seconds to give time to boot and write
	# an info.js file
	puts "--Waiting 90 seconds for FB4 to boot and write an info.js file"
	ebfarm_cmd pwr reboot
	exec sh -c "sleep 90"

	# Verify we commissioned the correct version, the expected version is
	# determined by the naming convention of the commissioning file, e.g.
	# 'fb4_commission_pack_v1.4.DAILY-2016-03-02-01-17-0000.zip' becomes
	# '1.4.DAILY-2016-03-02-01-17-0000'
	set expectedversion [exec sh -c "A=$path && B=\${A%.*} && C=\${B##*fb4_commission_pack_v} && echo \$C"]
	verify_fb4_version_via_info_js $expectedversion

	# Update to new version

	puts "--Updating FB4 with latest update image"
	set path [exec sh -c "ls dropbox/fb4_update_v*.tgz"]
	update_fb4 $path
	
	# Turning board on for 30 seconds to give time to boot and write
	# an info.js file
	puts "--Waiting 90 seconds for FB4 to boot and write an info.js file"
	ebfarm_cmd pwr reboot
	exec sh -c "sleep 90"

	# Verify we commissioned the correct version, the expected version is
	# determined by the naming convention of the commissioning file, e.g.
	# 'fb4_commission_pack_v1.4.DAILY-2016-03-02-01-17-0000.zip' becomes
	# '1.4.DAILY-2016-03-02-01-17-0000'
	set expectedversion [exec sh -c "A=$path && B=\${A%.*} && C=\${B##*fb4_update_v} && echo \$C"]
	verify_fb4_version_via_info_js $expectedversion
}

# Verify the correct version of software is running on the FB4 by reading
# the contents of the info.js file in RAM
proc verify_fb4_version_via_info_js {expectedversion} {
	puts "--Logging into board"
    log_into_board "~ #" "root" "oslfb4"

	puts "--Reading info.js to get version"
	send -s "cat /tmp/info/info.js\r"
	set timeout 150

	# Expect to see the current version in the info.js output
	expect {
		timeout {
			puts "--VERSION NOT AS EXPECTED"
			set timeout 120
			exit 1
		}
		"*\"$expectedversion\"*"
	}
	set timeout 120
}

# Verify the correct version of software is running on the FB4 by reading
# the contents of the info.js file on the SD card, this isn't used as it
# suffers from issues such as the SD card taking a long time to mount, fsck,
# buffercache issues etc.
proc verify_fb4_version_via_info_js_sdcard {expectedversion} {
	puts "--Turning board off, giving SD card to farm"
	ebfarm_cmd pwr off
	ebfarm_cmd sd host mount

	puts "--Figure out what the SD card is mounted"
	set output [ebfarm_cmd sd host]
	set mnt [exec sh -c "echo '$output' | grep ' at ' | rev | cut -d ' ' -f 1 | rev"]

	puts "--Determine version written to info.js"
	set sed1 [subst -nocommands -novariables { s/\\.\\0*\\([1-9]*\\)$/.\\1/ } ]
	set ver [exec sh -c "grep device_firmware_version $mnt/info.js  | cut -d '\"' -f 4 | sed '$sed1'"]

	puts "--Turning board off"
	ebfarm_cmd pwr off

	puts "--Expecting version $expectedversion"
	puts "--Version from info.js $ver"
	if { [string compare -nocase $expectedversion $ver] != 0 } {
		puts "--VERSION NOT AS EXPECTED"
		exit 1 
	}

	puts "--Giving SD card back to board"
	ebfarm_cmd sd board
}
