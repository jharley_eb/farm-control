#!/bin/bash
if [ -e ${BOARD_NAME}.sh ]; then
	source ${BOARD_NAME}.sh
else
	setup_board() { 
		echo "No board specific setup"
	}
	inter_test_cleanup() { 
		echo "No board specific inter-test function"
	}
	board_cleanup() { 
		echo "No board specific cleanup"
	}
fi

# Log all output to a log-summary.html file
exec > >(tee -i logs/log-summary.html)
exec 2>&1

# Calculate a checksum
CHKSUM=`md5sum ${FILE} | cut -d ' ' -f 1`

# Provide a friendly summary
echo "<p>Following is a summary of automated tests performed on `date -u +'%d %b %y at %H:%M'` against version ${VERSION} of the ${PROJECT} software (md5sum ${CHKSUM})." | fold -w 76 -s 
echo "</p>"

# Improve spacing in output
echo "<p/>"

# Call board specific setup function (e.g. unpack nfs, or flash the board)
setup_board

echo "<p>Running tests</p>"

# Execute the tests
TESTS_FAILED=0
for script in ???-*.tcl ; do
	# Extract from the script a comment describing the test
	TEST_NAME=`head -n 2 $script | tail -n 1 | tail -c +3`
	printf "%-50.50s" "$TEST_NAME (`basename $script`): "

	# Execute the test
	/usr/bin/annotate-output ./$script > logs/`basename $script`.log 2>&1
	STATUS=$?

	# Determine if it was successful
	if [ "$STATUS" = "0" ] ; then
		echo -e "Passed <br/>"
	else
		echo -e "FAILED <br/>"
		TESTS_FAILED=1
	fi
	inter_test_cleanup
done
echo

# Call board specific cleanup function
board_cleanup

# Ensure we exit 1 if tests failed
if [ "$TESTS_FAILED" == "1" ] ; then
	exit 1
fi
