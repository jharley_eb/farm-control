#!/usr/bin/expect

# Execute a farm command, this is a wrapper to the Embedded Bits SSH access
# and provides a similar interface to the normal container access
proc ebfarm_cmd {args} {
	puts "--FARM_CMD: $args"
	set output [exec bash -c "/usr/bin/ebfarm $args"]
	return $output
}

# Boot the board using the reboot command, wait for the 'usual' "login:" prompt
proc ebfarm_boot_board {boot_timeout} {
	# Handle custom timeout for slow boards
	if {"$boot_timeout" != ""} {
		set timeout $boot_timeout
	} else {
		set timeout 30
	}
	
	# Turn on board
	puts "--Waiting $timeout seconds for board to boot"
	ebfarm_cmd pwr reboot

	# Wait for login prompt
	expect {
		timeout {
			return 1
		}
		"*login: *"
	}

	return 0
}

# Function(s) to get a file from the board:
proc get_file_ssh {user ip path {localname ""}} {
	set output [exec scp -q -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no $user@$ip:$path ./$localname]
	return output
}

# Functions to login to board
proc get_prompt_and_login {user pass} {
	puts "--Logging into board"
	send -s "\r\r\r"
	expect {
		timeout {
			return 1
		}
		"*login: *"
	}

	send -s "$user\r"

	if {"$pass" != ""} {
		set timeout 5
		expect {
			timeout {
				puts "--No password prompt"
				send -s "\r"
				send -s "\r"
				return 1
			}
			"Password:"
		}

		send -s "$pass\r"
	}

	return 0	
}

# Log into a board
proc log_into_board {expected_prompt user pass} {
	get_prompt_and_login $user $pass

	expect {
		timeout {
			puts "--No command prompt, trying again"

			# Sometimes we see this we send the password but the 
			# board isn't ready to receive it, so we try again

			get_prompt_and_login $user $pass

			expect {
				timeout {
					puts "--No command prompt, giving up"
					return 1
				}
				"$expected_prompt"
			}
		}
		"$expected_prompt"
	}

	return 0
}

# All test scripts will use this file, all test scripts will likely want to
# parse output from the board so make that available
puts "--Setting up console"
set board_uart $::env(BOARD_UART)
puts "Board UART = $board_uart"

set baud 115200
exec sh -c "stty -F $board_uart ispeed $baud ospeed $baud raw -echo cs8 -parenb -cstopb -crtscts"
set spawned [spawn -open [open $board_uart w+]]
set timeout 120

set send_slow {1 .001}
