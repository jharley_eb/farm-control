#!/bin/bash

source $BOARD_PATH/usb

sudo stty -F /dev/`basename $USB_CONTROL` 9600

if [ "$5" = "on" ] ; then
	echo $USB_ON > /dev/`basename $USB_CONTROL`
elif [ "$5" = "off" ] ; then
	echo $USB_OFF > /dev/`basename $USB_CONTROL`
elif [ "$5" = "reboot" ] ; then
	echo $USB_OFF > /dev/`basename $USB_CONTROL`
	sleep 1
	echo $USB_ON > /dev/`basename $USB_CONTROL`
else
	log_user "Invalid arguments"
	exit 1
fi
