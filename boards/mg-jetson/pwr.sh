#!/bin/bash

source $BOARD_PATH/pwr

sudo stty -F /dev/`basename $PWR_BUT_CONTROL` 9600

CUR_STATE=$(snmpget -v 1 -c private ${PWR_IP} 1.3.6.1.4.1.318.1.1.4.4.2.1.3.${PWR_PORT} | cut -d' ' -f4)

if [ "$5" = "on" ] ; then
	if [ "$CUR_STATE" == "1" ]; then
		log_user "Board already on, doing nothing"
		exit 1
	fi
	echo $PWR_BUT_OFF > /dev/`basename $PWR_BUT_CONTROL`
	sleep 1
	snmpset -v 1 -c private $PWR_IP 1.3.6.1.4.1.318.1.1.4.4.2.1.3.$PWR_PORT integer 1
	sleep 1
	echo $PWR_BUT_ON > /dev/`basename $PWR_BUT_CONTROL`
	sleep 1
	echo $PWR_BUT_OFF > /dev/`basename $PWR_BUT_CONTROL`
elif [ "$5" = "off" ] ; then
	snmpset -v 1 -c private $PWR_IP 1.3.6.1.4.1.318.1.1.4.4.2.1.3.$PWR_PORT integer 2
	echo $PWR_BUT_OFF > /dev/`basename $PWR_BUT_CONTROL`
elif [ "$5" = "reboot" ] ; then
	snmpset -v 1 -c private $PWR_IP 1.3.6.1.4.1.318.1.1.4.4.2.1.3.$PWR_PORT integer 2
	echo $PWR_BUT_OFF > /dev/`basename $PWR_BUT_CONTROL`
	sleep 1
	snmpset -v 1 -c private $PWR_IP 1.3.6.1.4.1.318.1.1.4.4.2.1.3.$PWR_PORT integer 1
	sleep 1
	echo $PWR_BUT_ON > /dev/`basename $PWR_BUT_CONTROL`
	sleep 1
	echo $PWR_BUT_OFF > /dev/`basename $PWR_BUT_CONTROL`
else
	log_user "Invalid arguments"
	exit 1
fi
