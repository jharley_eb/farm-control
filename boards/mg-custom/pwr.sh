#!/bin/bash

source $BOARD_PATH/sd

sudo stty -F /dev/`basename $SDMUX_CONTROL` 9600

if [ "$5" = "on" ] ; then
	sudo sh -c "echo $SDMUX_BOARD_CMD > /dev/`basename $SDMUX_CONTROL`"
elif [ "$5" = "reboot" ] ; then
	sudo sh -c "echo $SDMUX_HOST_CMD > /dev/`basename $SDMUX_CONTROL`"
	log_user "Power button released"
	sleep 1.5
	sudo sh -c "echo $SDMUX_BOARD_CMD > /dev/`basename $SDMUX_CONTROL`"
	log_user "Power button pressed"

elif [ "$5" = "off" ] ; then
	sudo sh -c "echo $SDMUX_HOST_CMD > /dev/`basename $SDMUX_CONTROL`"
else
	log_user "Invalid arguments"
	exit 1
fi
