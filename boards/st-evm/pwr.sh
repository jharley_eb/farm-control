#!/bin/sh

source $BOARD_PATH/pwr

sudo stty -F /dev/`basename $PWR_CONTROL` 9600

if [ "$5" = "on" ] ; then
	sudo sh -c "echo $PWR_ON_CMD > /dev/`basename $PWR_CONTROL`"
elif [ "$5" = "reboot" ] ; then
	sudo sh -c "echo $PWR_OFF_CMD > /dev/`basename $PWR_CONTROL`"
	sleep 1
	sudo sh -c "echo $PWR_ON_CMD > /dev/`basename $PWR_CONTROL`"

elif [ "$5" = "off" ] ; then
	sudo sh -c "echo $PWR_OFF_CMD > /dev/`basename $PWR_CONTROL`"
else
	log_user "Invalid arguments"
	exit 1
fi
