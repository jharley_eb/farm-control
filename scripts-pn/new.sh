#!/bin/bash
sudo vzctl exec $CONTAINER_ID mkdir -p /$ENV_WORKSPACE
sudo vzctl exec $CONTAINER_ID mkdir -p /$ENV_WORKSPACE/nfs
sudo vzctl exec $CONTAINER_ID mkdir -p /$ENV_WORKSPACE/tftp

if [ -e $BOARD_PATH/sd.sh ]; then
	sudo vzctl exec $CONTAINER_ID mkdir -p /$ENV_WORKSPACE/media
fi

sudo vzctl exec $CONTAINER_ID mkdir -p /$ENV_WORKSPACE/tests
sudo vzctl exec $CONTAINER_ID ln -s /farm/testing/ebfarm-functions.tcl /$ENV_WORKSPACE/tests/ebfarm-functions.tcl
sudo vzctl exec $CONTAINER_ID ln -s /farm/testing/run.sh /$ENV_WORKSPACE/tests/run.sh

BOARD_NAME=$(echo ${2} | awk -F"-" '{print $1"-"$2}')

if [ -e /farm/scripts-cn/testing/board-specific/$BOARD_NAME-functions.tcl ]; then
	sudo vzctl exec $CONTAINER_ID ln -s /farm/testing/board-specific/$BOARD_NAME-functions.tcl /$ENV_WORKSPACE/tests/$BOARD_NAME-functions.tcl
fi

if [ -e /farm/scripts-cn/testing/board-specific/$BOARD_NAME.sh ]; then
	sudo vzctl exec $CONTAINER_ID ln -s /farm/testing/board-specific/$BOARD_NAME.sh /$ENV_WORKSPACE/tests/$BOARD_NAME.sh
fi

sudo vzctl exec $CONTAINER_ID chown -R $ENV_USER:$ENV_USER /$ENV_WORKSPACE
sudo vzctl exec $CONTAINER_ID chmod -R 755 /$ENV_WORKSPACE

sudo sh -c "echo $2 > /vz/root/$CONTAINER_ID/$ENV_WORKSPACE/.board"
