#!/bin/bash
source $BOARD_PATH/uart
UART_DEV=`cat $UART`
UART_NODE="uart-$ENV_BOARD"

# Give the UART access to the container (for any user) 
sudo vzctl set $CONTAINER_ID --devices c:$UART_DEV:rw --save

# (Re)create a suitable dev node for it - this is per user as it
# overcomes lock file issues
sudo rm /farm/scripts-cn/dev/$UART_NODE.$ENV_USER || true
sudo mknod /farm/scripts-cn/dev/$UART_NODE.$ENV_USER c `echo $UART_DEV | cut -d ':' -f1` `echo $UART_DEV | cut -d ':' -f2`

sudo chmod 777 /farm/scripts-cn/dev/$UART_NODE.$ENV_USER

sudo ln -sf /farm/dev/$UART_NODE.$ENV_USER /vz/root/$CONTAINER_ID/$ENV_WORKSPACE/uart-$ENV_BOARD-$ENV_USER

sudo ln -s /vz/root/$CONTAINER_ID/$ENV_WORKSPACE/nfs /farm/nfs/$ENV_BOARD
sudo ln -s /vz/root/$CONTAINER_ID/$ENV_WORKSPACE/tftp /var/lib/tftp/$ENV_BOARD

LOCAL_IP=`ifconfig eth0 | grep "inet addr" | cut -d ':' -f 2 | cut -d ' ' -f 1`

log_user -e "You are now using ${ENV_BOARD} - `cat $BOARDS_PATH/${ENV_BOARD}/description`";
log_user
log_user -e "NFS path: \t$LOCAL_IP:/farm/nfs/$ENV_BOARD/"
log_user -e "TFTP path: \t$LOCAL_IP:/$ENV_BOARD"
log_user
