#!/bin/bash

#log_user "Waiting for EMI lock"
#lockfile -r -1 /tmp/emi.lock

PWR_IP=10.150.7.201
source $BOARD_PATH/pwr

if [ "$5" = "on" ] ; then
	snmpset -v 1 -c private $PWR_IP 1.3.6.1.4.1.318.1.1.4.4.2.1.3.$PWR_PORT integer 1
	sleep 1 # snmpset are async
elif [ "$5" = "off" ] ; then
	snmpset -v 1 -c private $PWR_IP 1.3.6.1.4.1.318.1.1.4.4.2.1.3.$PWR_PORT integer 2
	sleep 1 # snmpset are async
elif [ "$5" = "reboot" ] ; then
	snmpset -v 1 -c private $PWR_IP 1.3.6.1.4.1.318.1.1.4.4.2.1.3.$PWR_PORT integer 3
	sleep 6 # snmpset are async
else
	log_user "Invalid arguments"
#	rm -f /tmp/emi.lock
	exit 1
fi
#rm -f /tmp/emi.lock
