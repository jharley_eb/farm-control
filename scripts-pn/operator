#!/bin/bash
set -e

exec 3>&1 4>&2 1>/tmp/farm.log.$$ 2>&1
set -x

ENV_USER=$1
ENV_BOARD=$2
ENV_WORKSPACE=$3
ENV_COMMAND=$4

BOARDS_PATH=/farm/boards/
BOARD_PATH=$BOARDS_PATH/$ENV_BOARD

CLIENT_IP=`echo $SSH_CLIENT | cut -d ' ' -f 1`
CONTAINER_ID=`sudo vzlist -H -t -o ctid,ip | grep $CLIENT_IP | cut -d ' ' -f 8`

function log_user {
	echo $@ >&3
}

function leave {
	log_user "Command Failed"
	logger "[FARM] FAILED_CMD: $CLIENT_IP ($CONTAINER_ID) - see /tmp/farm.log.$$"
}

trap leave ERR

logger "[FARM] $CLIENT_IP ($CONTAINER_ID) $@"

# Provide list of boards that a user has access to
if [ "$ENV_COMMAND" = "boards" ] ; then
	log_user "You have access to the following boards:"
	for path in $(ls $BOARDS_PATH);
		do [ -f "$BOARDS_PATH/${path}/users/$ENV_USER" ] || continue;
		log_user -e " -> ${path}:     \t`cat $BOARDS_PATH/${path}/description`";
	done
	rm /tmp/farm.log.$$
	exit 0
fi

# All commands operate on a board - make sure the board is one that exists
if [ ! -d "$BOARD_PATH" ] ; then
	log_user "You do not have permission to access this board or it doesn't exist"
	rm /tmp/farm.log.$$
	exit 1
fi

# Ensure a user has permission to use the specific board
if [ ! -f "$BOARD_PATH/users/$ENV_USER" ] ; then
	log_user "You do not have permission to access this board or it doesn't exist"
	rm /tmp/farm.log.$$
	exit 1
fi


if [ "$ENV_COMMAND" = "new" ] ; then
	source $BOARD_PATH/$ENV_COMMAND.sh
	rm /tmp/farm.log.$$
	exit 0
fi

if [ "$ENV_COMMAND" = "use" ] ; then
	if [ -f "$BOARD_PATH/.user" ] ; then
		if [ "$ENV_USER" != "`cat $BOARD_PATH/.user`" ] ; then
			if [ "$5" == "force" ] ; then
				CURRENT_WORKSPACE=`cat $BOARD_PATH/.workspace`
				SHORT=${CURRENT_WORKSPACE#/vz/root/100/}
				/farm/scripts-pn/operator `cat $BOARD_PATH/.user` $ENV_BOARD $SHORT release >&3
				log_user "Forcing use of the board"
			elif [ "$5" == "wait" ] ; then
				log_user "Notifying `cat $BOARD_PATH/.user` that you are waiting"
				echo "$ENV_USER is currently waiting to use your board ($ENV_BOARD) - please release it if you are not using it" | mail -s "Your board is in demand" "`cat $BOARD_PATH/.user`@embedded-bits.co.uk"
				log_user "Waiting for board to become available..."
				while [ -f "$BOARD_PATH/.user" ] ; do
					sleep 10
					log_user "Still waiting..."
				done
			else
				log_user "This board is currently in use by someone else (`cat $BOARD_PATH/.user`)"
				log_user "Use the 'force' flag to force use"
				log_user "Use the 'wait' flag to wait"
				rm /tmp/farm.log.$$
				exit 1
			fi
		else
			if [ "/vz/root/$CONTAINER_ID/$ENV_WORKSPACE" == "`cat $BOARD_PATH/.workspace`" ] ; then
				LOCAL_IP=`ifconfig eth0 | grep "inet addr" | cut -d ':' -f 2 | cut -d ' ' -f 1`

				log_user "You are already using this board in this workspace, NFS/TFTP details are:"
				log_user
				log_user -e "NFS path: \t$LOCAL_IP:/farm/nfs/$ENV_BOARD/"
				log_user -e "TFTP path: \t$LOCAL_IP:/$ENV_BOARD"
				log_user
				rm /tmp/farm.log.$$
				exit 0
			else
				log_user "You are already using this board (perhaps in another workspace?)"
				rm /tmp/farm.log.$$
				exit 1
			fi
		fi
	fi

	echo $ENV_USER > /$BOARD_PATH/.user
	echo /vz/root/$CONTAINER_ID/$ENV_WORKSPACE > /$BOARD_PATH/.workspace
fi


if [ ! -f "$BOARD_PATH/.user" ] || [ "$ENV_USER" != "`cat $BOARD_PATH/.user`" ] ; then
	log_user "Please run the use command to execute commands"
	rm /tmp/farm.log.$$
	exit 1
fi

if [ "/vz/root/$CONTAINER_ID/$ENV_WORKSPACE" != "`cat $BOARD_PATH/.workspace`" ] ; then
	log_user "You are currently using this board but in another workspace, please use that instead"
	echo "/vz/root/$CONTAINER_ID/$ENV_WORKSPACE"
	echo "`cat $BOARD_PATH/.workspace`"
	rm /tmp/farm.log.$$
	exit 1
fi

if [ "$ENV_COMMAND" = "release" ] ; then

	/farm/scripts-pn/operator $ENV_USER $ENV_BOARD $ENV_WORKSPACE pwr off >&3
	/farm/scripts-pn/operator $ENV_USER $ENV_BOARD $ENV_WORKSPACE sd board >&3

	rm $BOARD_PATH/.user
	rm $BOARD_PATH/.workspace
fi

if [ ! -f "$BOARD_PATH/$ENV_COMMAND.sh" ] ; then
	log_user "Invalid command"
	rm /tmp/farm.log.$$
	exit 1
fi

source $BOARD_PATH/$ENV_COMMAND.sh
log_user "Command Success"
rm /tmp/farm.log.$$
