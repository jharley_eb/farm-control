#!/bin/bash

source $BOARD_PATH/sd

BLOCK=`ls $SDMUX_MEDIA/block`
ERROR=0

sudo stty -F /dev/`basename $SDMUX_CONTROL` 9600

if [ "$5" = "host" ] ; then
	sudo sh -c "echo $SDMUX_HOST_CMD > /dev/`basename $SDMUX_CONTROL`"

	PAT="$SDMUX_MEDIA/../../../../"
	BASE=`cd $PAT && basename $PWD`
	if [ -d "/sys/bus/usb/drivers/usb/$BASE" ] ; then
		log_user "Host already has SD card"
	else
		sudo sh -c "echo $BASE > /sys/bus/usb/drivers/usb/bind"
		sudo sh -c "cd $PAT && echo auto > power/level" || true
	fi

	if [ "$6" != "nowait" ] ; then

		log_user "Waiting for SD card ($BLOCK) to appear..."
		until cat /proc/partitions | grep $BLOCK ; do sleep 0.5 ; done
		log_user "Done"

		PARTS=`find $SDMUX_MEDIA/block/$BLOCK/ -name $BLOCK* | xargs -L 1 basename`
		NUMPARTS=$(echo $PARTS | wc -w)

		for DEV in $PARTS ; do
			sudo vzctl set $CONTAINER_ID --devnodes $DEV:rw --save
			sudo vzctl exec $CONTAINER_ID chown $ENV_USER:$ENV_USER /dev/$DEV
		done

		if [ "$6" != "nomount" ] ; then
			FAILPARTS=0
			for DEV in $PARTS ; do
				RET=0
				sudo mountpoint /vz/root/$CONTAINER_ID/$ENV_WORKSPACE/media/$DEV || RET=$?
				if [ "$RET" = "0" ] ; then
					FSTYPE=`sudo findmnt  -n -o FSTYPE -T  /media/$DEV`
					log_user "$DEV already mounted ($FSTYPE) at $ENV_WORKSPACE/media/$DEV"
				else 
					RET=0
					sudo pmount -u 000 $DEV || RET=$?
					if [ "$RET" = "0" ] ; then
						FSTYPE=`sudo findmnt  -n -o FSTYPE -T  /media/$DEV`
						sudo mkdir -p /vz/root/$CONTAINER_ID/$ENV_WORKSPACE/media/$DEV
						sudo mount --bind /media/$DEV /vz/root/$CONTAINER_ID/$ENV_WORKSPACE/media/$DEV
						log_user "$DEV ($FSTYPE) mounted at $ENV_WORKSPACE/media/$DEV"
					else
						ERROR=$RET
						((FAILPARTS+=1))
						log_user "$DEV failed to mount"
					fi
				fi
			done

			if [ $FAILPARTS -eq $NUMPARTS ]; then
				log_user "WARNING: No mountable partitions found!"
				exit $ERROR
			fi
		fi
	fi

elif [ "$5" = "board" ] ; then
	PARTS=`find $SDMUX_MEDIA/block/$BLOCK/ -name $BLOCK* | xargs -L 1 basename`

	for DEV in $PARTS ; do
		RET=0
		sudo mountpoint /vz/root/$CONTAINER_ID/$ENV_WORKSPACE/media/$DEV || RET=$?
		if [ "$RET" = "0" ] ; then
			sudo umount /vz/root/$CONTAINER_ID/$ENV_WORKSPACE/media/$DEV
		fi
		RET=0
		sudo mountpoint /media/$DEV || RET=$?
		if [ "$RET" = "0" ] ; then
			sudo pumount $DEV #need to be in group plugdev
		fi
		# This overcomes an issue Will saw where sometimes he couldn't mkfs.vfat because the device was busy
		# or if you remove the SD card from the sdmux without first unmounting in the container
		sudo umount /dev/$DEV || true
	done

	for DEV in $PARTS ; do
		sudo vzctl set $CONTAINER_ID --devnodes $DEV:none --save
	done
	
	PAT="$SDMUX_MEDIA/../../../../"
	BASE=`cd $PAT && basename $PWD`
	if [ -d "/sys/bus/usb/drivers/usb/$BASE" ] ; then
		sudo sh -c "cd $PAT && echo suspend > power/level"
		sudo sh -c "echo $BASE > /sys/bus/usb/drivers/usb/unbind"
	else
		log_user "Board already has SD card"
	fi

	sudo sh -c "echo $SDMUX_BOARD_CMD > /dev/`basename $SDMUX_CONTROL`"
elif [ "$5" = "stat" ] ; then
	log_user "$BLOCK"
elif [ "$5" = "snapshot" ] ; then
	if [ "$6" = "save" ] ; then
		sudo mkdir -p /vz/root/$CONTAINER_ID/$ENV_WORKSPACE/snapshots/$7/
		sudo rsync -av --delete /vz/root/$CONTAINER_ID/$ENV_WORKSPACE/media/ /vz/root/$CONTAINER_ID/$ENV_WORKSPACE/snapshots/$7/
	elif [ "$6" = "restore" ] ; then
		sudo rsync -av --delete /vz/root/$CONTAINER_ID/$ENV_WORKSPACE/snapshots/$7/ /vz/root/$CONTAINER_ID/$ENV_WORKSPACE/media/
	fi
else
	log_user "Invalid arguments"
	exit 1
fi
