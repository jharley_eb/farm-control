#!/bin/bash
BOARDS_PATH=/farm/boards

for CT in $(sudo vzlist -H -o ctid); do echo "CT $CT: `sudo vzctl exec $CT w`"; echo "" ; done

echo ""

for path in $(ls $BOARDS_PATH); do
	if [ -f "$BOARDS_PATH/${path}/.user" ] ; then
		echo -e "${path}:	\t`cat $BOARDS_PATH/${path}/.user`:`cat $BOARDS_PATH/${path}/.workspace`";
	else
		echo -e "${path}:	\tNot in use";
	fi
done

echo ""

tail /var/log/farm.log
