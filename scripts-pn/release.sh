#!/bin/bash
source $BOARD_PATH/uart

UART_DEV=`cat $UART`
UART_NODE="uart-$ENV_BOARD"

UART_USERS=`sudo lsof -t /vz/root/$CONTAINER_ID/farm/dev/$UART_NODE.$ENV_USER || true`

sudo vzctl set $CONTAINER_ID --devices c:$UART_DEV:none --save

while read -r line ; do
	UART_USER=$line
	for FD in `sudo ls /proc/$UART_USER/fd/` ; do
		if [ "`sudo readlink /proc/$UART_USER/fd/$FD`" = "/vz/root/$CONTAINER_ID/farm/dev/$UART_NODE.$ENV_USER" ] ; then
			echo "GOT YOU $UART_USER:$FD"
			echo "p close($FD)" > /tmp/.$$.tmp
			echo "detach" >> /tmp/.$$.tmp
			echo "quit" >> /tmp/.$$.tmp
			sudo gdb -p $UART_USER < /tmp/.$$.tmp
			rm /tmp/.$$.tmp
		fi
	done
done <<< "$UART_USERS"

sudo rm /farm/scripts-cn/dev/$UART_NODE.$ENV_USER || true

sudo rm /farm/nfs/$ENV_BOARD
sudo rm /var/lib/tftp/$ENV_BOARD
