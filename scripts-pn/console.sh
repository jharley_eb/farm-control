#!/bin/bash
source $BOARD_PATH/uart
DEV_NAME=`ls $UART`
DIR_NAME=`dirname $DEV_NAME`
BASE_NAME=`basename $DIR_NAME`

if [ "$5" == "" ] ; then
	log_user "No baud rate given"
	exit 1;
fi

exec 1>&3 2>&4
sg dialout -c "picocom --send-cmd '' --receive-cmd '' -b $5 /dev/$BASE_NAME"
exec 1>/tmp/farm.log.$$ 2>&1
