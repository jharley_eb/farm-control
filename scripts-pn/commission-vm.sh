#!/bin/bash
CTID=$1
IP=$2

TEMPLATE=ubuntu-14.04-x86_64-minimal

if [ "$#" != "2" ] ; then
	echo "Usage $0 <ctid> <ip>"
	exit 1
fi

vzctl create $CTID --ostemplate $TEMPLATE
vzctl set $CTID --onboot yes --save
vzctl set $CTID --hostname ebfarm.$CTID --save
vzctl set $CTID --userpasswd root:ebfarmroot
vzctl set $CTID --diskspace 9G:10G
vzctl start $CTID
sudo mkdir -p /vz/root/$CTID/farm
sudo ln -s /farm/ebfarm /vz/root/$CTID/usr/bin/ebfarm

# TODO this is still in use
vzctl set $CTID --ipadd 10.150.7.198
vzctl exec $CTID apt-get update
vzctl exec $CTID apt-get install -y minicom
vzctl exec $CTID apt-get install -y rsync 

vzctl set $CTID --ipadd $IP --save
vzctl stop $CTID
vzctl start $CTID
sudo mount --bind -r /farm/scripts-cn /vz/root/$CTID/farm
