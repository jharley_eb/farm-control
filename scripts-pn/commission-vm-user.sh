#!/bin/bash
CTID=$1
USERNAME=$2
PASS=$3
CLIENT_KEY=$4

if [ "$#" != "4" ] ; then
    echo "Usage $0 <ctid> <username> <pass> <clientsshkey>"
    exit 1
fi

THISUSER=`whoami`
if [ "$THISUSER" != "root" ]; then
    echo "root permissions are needed to add a new user"
    exit 1
fi

SSH_CONFIG="/farm/scripts-pn/media/config"
SSH_KNOWN_HOSTS="/farm/scripts-pn/media/known_hosts"
if [ ! -e "${SSH_CONFIG}" -o ! -e "${SSH_KNOWN_HOSTS}" ]; then
    echo "Files missing, aborting"
    echo 1
fi

vzctl exec $CTID adduser --disabled-password --gecos "" $USERNAME 
vzctl set $CTID --userpasswd $USERNAME:$PASS

mkdir -p /vz/root/$CTID/home/$USERNAME/.ssh/
ssh-keygen -N "" -t rsa -f /vz/root/$CTID/home/$USERNAME/.ssh/id_rsa
vzctl exec $CTID mkdir -p /home/$USERNAME/.ssh/control
cp ${SSH_CONFIG} /vz/root/$CTID/home/$USERNAME/.ssh/config
cp ${SSH_KNOWN_HOSTS} /vz/root/$CTID/home/$USERNAME/.ssh/known_hosts
vzctl exec $CTID chmod 664 /home/$USERNAME/.ssh/config
vzctl exec $CTID chmod 664 /home/$USERNAME/.ssh/known_hosts
vzctl exec $CTID chmod 700 /home/$USERNAME/.ssh/
vzctl exec $CTID chown -R $USERNAME:$USERNAME /home/$USERNAME/.ssh/

echo "no-agent-forwarding,no-port-forwarding,no-user-rc,no-X11-forwarding,command=\"/farm/scripts-pn/operator $USERNAME \$SSH_ORIGINAL_COMMAND\" `cat /vz/root/$CTID/home/$USERNAME/.ssh/id_rsa.pub`" >> /home/op/.ssh/authorized_keys

cat $CLIENT_KEY > /vz/root/$CTID/home/$USERNAME/.ssh/authorized_keys

# Add rsync line to end of sudoers
echo $USERNAME " ALL=NOPASSWD:/usr/bin/rsync" >> /vz/root/$CTID/etc/sudoers
